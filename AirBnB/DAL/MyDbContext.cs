﻿using AirBnB.Common;
using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AirBnB.DAL
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("MyDbContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Amenity> Amenities { get; set; }
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //da tabele budu imenovane User, Booking etc. umesto Users, Bookings            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Entity<User>().MapToStoredProcedures();
            //modelBuilder.Entity<User>().HasTableAnnotation("SoftDelete", true);
            //modelBuilder.Entity<User>().UseSoftDelete();
            modelBuilder.Entity<User>().MapToStoredProcedures();

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var item in this.ChangeTracker.Entries())
            {
                if (item.State == EntityState.Deleted && item.Entity is ISoftDelete)
                {
                    //item.CurrentValues[nameof(ISoftDelete.IsDeleted)] = true;
                    item.State = EntityState.Modified;
                }
            }

            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            foreach (var item in this.ChangeTracker.Entries())
            {
                if (item.State == EntityState.Deleted && item.Entity is ISoftDelete)
                {
                    //item.CurrentValues[nameof(ISoftDelete.IsDeleted)] = true;
                    item.State = EntityState.Modified;
                }
            }
            return base.SaveChangesAsync();
        }

    }
}