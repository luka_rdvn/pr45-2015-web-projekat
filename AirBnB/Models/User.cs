﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AirBnB.Models.enums;

namespace AirBnB.Models
{
    public class User : ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(20)]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Name cannot contain numbers.")] 
        public string Name { get; set; }

        [Required]
        [StringLength(20)]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Last name cannot contain numbers.")]  
        [Display(Name = "Last Name")]
        public string Surname { get; set; }

        [Required]
        public Gender Gender { get; set; }

        public Role Role { get; set; } = Role.Guest;
        //ako je Host
        public List<Apartment> ApartmentsForRent { get; set; }
        //ako je Guest
        public List<Apartment> RentedApartments { get; set; }
        public List<Booking> Bookings { get; set; }

        public bool IsDeleted { get; set; }

        public User()
        {
            ApartmentsForRent = new List<Apartment>();
            RentedApartments = new List<Apartment>();
            Bookings = new List<Booking>();
        }
    }
}