﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirBnB.Models.enums
{
    public enum ApartmentType
    {
        EntirePlace,
        Room
    }
}