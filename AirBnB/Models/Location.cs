﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirBnB.Models
{
    public class Location : ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Range(0, 90, ErrorMessage = "Latitude must be between 0 and 90.")]
        public double Latitude { get; set; }
        [Required]
        [Range(0, 180, ErrorMessage = "Longitude must be between 0 and 180.")]
        public double Longitude { get; set; }
        [Required]
        public Address Address { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}