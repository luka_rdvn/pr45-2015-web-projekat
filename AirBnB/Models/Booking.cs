﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AirBnB.Models.enums;

namespace AirBnB.Models
{
    public class Booking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int  Id { get; set; }
        public Apartment Apartment { get; set; }
        public DateTime StartDate { get; set; } //gost moze da rezervise niz slobodnih datuma samo ako oni nemaju prekida
        public int NumberOfNights { get; set; } = 1;
        public double TotalPrice { get; set; }
        public User Guest { get; set; }
        public BookingStatus Status { get; set; }
    }
}