﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirBnB.Models
{
    public class Review : ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public User Guest { get; set; }
        public Apartment Apartment { get; set; }
        [Required]
        public string Comment { get; set; }
        [Required]
        [Range(1, 5, ErrorMessage = "Rating must be between 1 and 5.")]
        [Display(Name = "Rating(1-5)")]
        public double Rating { get; set; }

        public bool IsHidden { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
    }
}