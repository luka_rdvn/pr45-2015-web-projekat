﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AirBnB.Models.enums;

namespace AirBnB.Models
{
    public class Apartment : ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public ApartmentType Type { get; set; }
        [Required]
        [Display(Name = "Number of rooms")]
        [Range(0, 15)]
        public int NumberOfRooms { get; set; }
        [Required]
        [Display(Name = "Number of guests")]
        [Range(1, 30)]
        public int NumberOfGuests { get; set; }
        [Required]
        public Location Location { get; set; }
        public List<DateTime> DatesForRent { get; set; } //postavljaju Host-ovi
        public List<DateTime> AvailableDates { get; set; } //sistem automatski generise slobodne datume
        public User Host { get; set; }
        public List<Review> Reviews { get; set; }
        public List<Image> Images { get; set; }
        [Required]
        [Display(Name = "Price in USD")]
        public double Price { get; set; }
        [Required]
        [RegularExpression(@"((1[0-2]|0?[1-9])?([AaPp][Mm]))", ErrorMessage = "Wrong format, use AM/PM")]
        public string CheckIn { get; set; } = "2PM";    //string?
        [Required]
        [RegularExpression(@"((1[0-2]|0?[1-9])?([AaPp][Mm]))", ErrorMessage = "Wrong format, use AM/PM")]
        public string CheckOut { get; set; } = "10AM";
        [Required]
        public ApartmentStatus Status { get; set; }
        public List<Amenity> Amenities { get; set; }
        public List<Booking> Bookings { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}