﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirBnB.Models
{
    public class Address : ISoftDelete
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string Number { get; set; } //because number can be 2A for example
        [Required]
        public string City { get; set; }
        [Required]
        [DataType(DataType.PostalCode)]
        public int PostalCode { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}