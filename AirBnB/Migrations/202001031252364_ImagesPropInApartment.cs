﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ImagesPropInApartment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Image", "Apartment_Id", c => c.Int());
            CreateIndex("dbo.Image", "Apartment_Id");
            AddForeignKey("dbo.Image", "Apartment_Id", "dbo.Apartment", "Id");
            DropColumn("dbo.Apartment", "Pictures");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Apartment", "Pictures", c => c.String());
            DropForeignKey("dbo.Image", "Apartment_Id", "dbo.Apartment");
            DropIndex("dbo.Image", new[] { "Apartment_Id" });
            DropColumn("dbo.Image", "Apartment_Id");
        }
    }
}
