﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateAmenities : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO Amenity(Name)
                  VALUES ('WiFi'), ('Washer'), ('Dryer'), ('AC'), ('TV'), ('Heating'), ('Essentials'), ('Hot water'),
                         ('Iron'), ('Crib'), ('High Chair'), ('Elevator'), ('Free Parking'), ('Kitchen'), ('Microwave')");
        }
        
        public override void Down()
        {
        }
    }
}
