﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImgName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Image", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Image", "Name");
        }
    }
}
