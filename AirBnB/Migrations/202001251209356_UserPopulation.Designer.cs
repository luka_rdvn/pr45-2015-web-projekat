﻿// <auto-generated />
namespace AirBnB.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class UserPopulation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UserPopulation));
        
        string IMigrationMetadata.Id
        {
            get { return "202001251209356_UserPopulation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
