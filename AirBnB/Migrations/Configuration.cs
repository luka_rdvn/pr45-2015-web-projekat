﻿namespace AirBnB.Migrations
{
    using AirBnB.Common;
    using AirBnB.Models;
    using AirBnB.Models.enums;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AirBnB.DAL.MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AirBnB.DAL.MyDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            /*context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('User', RESEED, 0)");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Address', RESEED, 0)");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Location', RESEED, 0)");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Apartment', RESEED, 0)");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Amenity', RESEED, 0)");
            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Review', RESEED, 0)");

            context.Users.AddOrUpdate(x => x.Id,
                new User() { IsDeleted = false, Gender = Gender.Female, Name = "Zorica", Surname = "Zoric", Password = "zorica", Username = "zorica", Role = Role.Host },
                new User() { IsDeleted = false, Gender = Gender.Male, Name = "Domacin", Surname = "Domacin", Password = "domacin", Username = "domacin", Role = Role.Host },
                new User() { IsDeleted = false, Gender = Gender.Male, Name = "Dragan", Surname = "Radonjic", Password = "dragan", Username = "dragan", Role = Role.Host },
                new User() { IsDeleted = false, Gender = Gender.Female, Name = "Anja", Surname = "Markovic", Password = "anja", Username = "anja", Role = Role.Guest },
                new User() { IsDeleted = false, Gender = Gender.Male, Name = "Danilo", Surname = "Galinari", Password = "danilo", Username = "danilo", Role = Role.Guest },
                new User() { IsDeleted = false, Gender = Gender.Male, Name = "Strahinja", Surname = "Krcmar", Password = "strahinja", Username = "strahinja", Role = Role.Guest },
                new User() { IsDeleted = false, Gender = Gender.Female, Name = "Tamara", Surname = "Matic", Password = "gost", Username = "gost", Role = Role.Guest },
                new User() { IsDeleted = false, Gender = Gender.Male, Name = "Darko", Surname = "Milicic", Password = "darko", Username = "darko", Role = Role.Guest }
            );

            context.Addresses.AddOrUpdate(x => x.Id,
                new Address() { IsDeleted = false, City = "Novi Sad", Street = "Safarikova", Number = "12", PostalCode = 21000 },
                new Address() { IsDeleted = false, City = "Zrenjanin", Street = "Njegoseva", Number = "2", PostalCode = 23000 },
                new Address() { IsDeleted = false, City = "Sombor", Street = "Kralja Petra I", Number = "88A", PostalCode = 21000 },
                new Address() { IsDeleted = false, City = "Ledinci", Street = "Nikolin do", Number = "2", PostalCode = 21207 },
                new Address() { IsDeleted = false, City = "Kac", Street = "Vojvode Stepe", Number = "55", PostalCode = 20085 },
                new Address() { IsDeleted = false, City = "Beograd", Street = "Ljutice Bogdana", Number = "2A", PostalCode = 11000 }
            );

            context.Locations.AddOrUpdate(x => x.Id,
                new Location() { Address = context.Addresses.Find(1), IsDeleted = false, Latitude = 80, Longitude = 80 },
                new Location() { Address = context.Addresses.Find(2), IsDeleted = false, Latitude = 85, Longitude = 80 },
                new Location() { Address = context.Addresses.Find(3), IsDeleted = false, Latitude = 70, Longitude = 76 },
                new Location() { Address = context.Addresses.Find(4), IsDeleted = false, Latitude = 45, Longitude = 80 },
                new Location() { Address = context.Addresses.Find(5), IsDeleted = false, Latitude = 68, Longitude = 92 },
                new Location() { Address = context.Addresses.Find(6), IsDeleted = false, Latitude = 28, Longitude = 80 }
            );

            context.Apartments.AddOrUpdate(x => x.Id,
                new Apartment() { CheckIn = "3PM", CheckOut = "11AM", Host = context.Users.Find(1), IsDeleted = false, Location = context.Locations.Find(1), Price = 140, NumberOfGuests = 5, NumberOfRooms = 2, Status = ApartmentStatus.Active, Type = ApartmentType.EntirePlace },
                new Apartment() { CheckIn = "4PM", CheckOut = "11AM", Host = context.Users.Find(1), IsDeleted = false, Location = context.Locations.Find(2), Price = 120, NumberOfGuests = 4, NumberOfRooms = 2, Status = ApartmentStatus.Inactive, Type = ApartmentType.EntirePlace },
                new Apartment() { CheckIn = "3PM", CheckOut = "11AM", Host = context.Users.Find(2), IsDeleted = false, Location = context.Locations.Find(3), Price = 100, NumberOfGuests = 3, NumberOfRooms = 1, Status = ApartmentStatus.Inactive, Type = ApartmentType.Room },
                new Apartment() { CheckIn = "2PM", CheckOut = "10AM", Host = context.Users.Find(2), IsDeleted = false, Location = context.Locations.Find(4), Price = 200, NumberOfGuests = 10, NumberOfRooms = 4, Status = ApartmentStatus.Inactive, Type = ApartmentType.EntirePlace },
                new Apartment() { CheckIn = "3PM", CheckOut = "11AM", Host = context.Users.Find(3), IsDeleted = false, Location = context.Locations.Find(5), Price = 160, NumberOfGuests = 8, NumberOfRooms = 3, Status = ApartmentStatus.Active, Type = ApartmentType.EntirePlace },
                new Apartment() { CheckIn = "2PM", CheckOut = "10AM", Host = context.Users.Find(3), IsDeleted = false, Location = context.Locations.Find(6), Price = 110, NumberOfGuests = 5, NumberOfRooms = 3, Status = ApartmentStatus.Active, Type = ApartmentType.EntirePlace }
            );

            context.Amenities.AddOrUpdate(x => x.Id,
                new Amenity() { IsDeleted = false, Name = "WiFi"},
                new Amenity() { IsDeleted = false, Name = "Kitchen" },
                new Amenity() { IsDeleted = false, Name = "Parking" },
                new Amenity() { IsDeleted = false, Name = "A/C" },
                new Amenity() { IsDeleted = false, Name = "TV" },
                new Amenity() { IsDeleted = false, Name = "Landline" },
                new Amenity() { IsDeleted = false, Name = "Washer" },
                new Amenity() { IsDeleted = false, Name = "Dryer" },
                new Amenity() { IsDeleted = false, Name = "Towels" },
                new Amenity() { IsDeleted = false, Name = "Pool" }
            );

            context.Reviews.AddOrUpdate(x => x.Id,
                new Review() { Guest = context.Users.Find(4), Apartment = context.Apartments.Find(1), IsDeleted = false, IsHidden = false, Rating = 4.5, Comment = "Bilo je sjajno."},
                new Review() { Guest = context.Users.Find(4), Apartment = context.Apartments.Find(2), IsDeleted = false, IsHidden = false, Rating = 2.5, Comment = "Bilo je uzasno." },
                new Review() { Guest = context.Users.Find(5), Apartment = context.Apartments.Find(3), IsDeleted = false, IsHidden = false, Rating = 5.0, Comment = "Bilo je fenomenalno." },
                new Review() { Guest = context.Users.Find(6), Apartment = context.Apartments.Find(3), IsDeleted = false, IsHidden = false, Rating = 3.7, Comment = "Moze da prodje." },
                new Review() { Guest = context.Users.Find(7), Apartment = context.Apartments.Find(3), IsDeleted = false, IsHidden = false, Rating = 4.0, Comment = "Skroz zadovoljavajuce." },
                new Review() { Guest = context.Users.Find(8), Apartment = context.Apartments.Find(4), IsDeleted = false, IsHidden = false, Rating = 2.0, Comment = "Katastrofalno." }
            );*/
        }
    }
}
