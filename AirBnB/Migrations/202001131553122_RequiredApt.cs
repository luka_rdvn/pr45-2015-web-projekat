﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredApt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Apartment", "Location_Id", "dbo.Location");
            DropIndex("dbo.Apartment", new[] { "Location_Id" });
            AlterColumn("dbo.Apartment", "CheckIn", c => c.String(nullable: false));
            AlterColumn("dbo.Apartment", "CheckOut", c => c.String(nullable: false));
            AlterColumn("dbo.Apartment", "Location_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Apartment", "Location_Id");
            AddForeignKey("dbo.Apartment", "Location_Id", "dbo.Location", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Apartment", "Location_Id", "dbo.Location");
            DropIndex("dbo.Apartment", new[] { "Location_Id" });
            AlterColumn("dbo.Apartment", "Location_Id", c => c.Int());
            AlterColumn("dbo.Apartment", "CheckOut", c => c.String());
            AlterColumn("dbo.Apartment", "CheckIn", c => c.String());
            CreateIndex("dbo.Apartment", "Location_Id");
            AddForeignKey("dbo.Apartment", "Location_Id", "dbo.Location", "Id");
        }
    }
}
