﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Street = c.String(),
                        Number = c.String(),
                        City = c.String(),
                        PostalCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Amenity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Apartment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartment", t => t.Apartment_Id)
                .Index(t => t.Apartment_Id);
            
            CreateTable(
                "dbo.Apartment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        NumberOfRooms = c.Int(nullable: false),
                        NumberOfGuests = c.Int(nullable: false),
                        Pictures = c.String(),
                        Price = c.Double(nullable: false),
                        CheckIn = c.String(),
                        CheckOut = c.String(),
                        Status = c.Int(nullable: false),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                        Host_Id = c.Int(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .ForeignKey("dbo.User", t => t.Host_Id)
                .ForeignKey("dbo.Location", t => t.Location_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1)
                .Index(t => t.Host_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Booking",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        NumberOfNights = c.Int(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        Status = c.Int(nullable: false),
                        Apartment_Id = c.Int(),
                        Guest_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartment", t => t.Apartment_Id)
                .ForeignKey("dbo.User", t => t.Guest_Id)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Guest_Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Gender = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Address_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.Address_Id)
                .Index(t => t.Address_Id);
            
            CreateTable(
                "dbo.Review",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comment = c.String(),
                        Rating = c.Double(nullable: false),
                        Apartment_Id = c.Int(),
                        Guest_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartment", t => t.Apartment_Id)
                .ForeignKey("dbo.User", t => t.Guest_Id)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Guest_Id);
            
            CreateTable(
                "dbo.Image",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Review", "Guest_Id", "dbo.User");
            DropForeignKey("dbo.Review", "Apartment_Id", "dbo.Apartment");
            DropForeignKey("dbo.Apartment", "Location_Id", "dbo.Location");
            DropForeignKey("dbo.Location", "Address_Id", "dbo.Address");
            DropForeignKey("dbo.Apartment", "Host_Id", "dbo.User");
            DropForeignKey("dbo.Apartment", "User_Id1", "dbo.User");
            DropForeignKey("dbo.Booking", "Guest_Id", "dbo.User");
            DropForeignKey("dbo.Apartment", "User_Id", "dbo.User");
            DropForeignKey("dbo.Booking", "Apartment_Id", "dbo.Apartment");
            DropForeignKey("dbo.Amenity", "Apartment_Id", "dbo.Apartment");
            DropIndex("dbo.Review", new[] { "Guest_Id" });
            DropIndex("dbo.Review", new[] { "Apartment_Id" });
            DropIndex("dbo.Location", new[] { "Address_Id" });
            DropIndex("dbo.Booking", new[] { "Guest_Id" });
            DropIndex("dbo.Booking", new[] { "Apartment_Id" });
            DropIndex("dbo.Apartment", new[] { "Location_Id" });
            DropIndex("dbo.Apartment", new[] { "Host_Id" });
            DropIndex("dbo.Apartment", new[] { "User_Id1" });
            DropIndex("dbo.Apartment", new[] { "User_Id" });
            DropIndex("dbo.Amenity", new[] { "Apartment_Id" });
            DropTable("dbo.Image");
            DropTable("dbo.Review");
            DropTable("dbo.Location");
            DropTable("dbo.User");
            DropTable("dbo.Booking");
            DropTable("dbo.Apartment");
            DropTable("dbo.Amenity");
            DropTable("dbo.Address");
        }
    }
}
