﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "Username", c => c.String(nullable: false));
            AlterColumn("dbo.User", "Name", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.User", "Surname", c => c.String(nullable: false, maxLength: 20));
            AlterStoredProcedure(
                "dbo.User_Insert",
                p => new
                    {
                        Username = p.String(),
                        Password = p.String(),
                        Name = p.String(maxLength: 20),
                        Surname = p.String(maxLength: 20),
                        Gender = p.Int(),
                        Role = p.Int(),
                        IsDeleted = p.Boolean(),
                    },
                body:
                    @"INSERT [dbo].[User]([Username], [Password], [Name], [Surname], [Gender], [Role], [IsDeleted])
                      VALUES (@Username, @Password, @Name, @Surname, @Gender, @Role, @IsDeleted)
                      
                      DECLARE @Id int
                      SELECT @Id = [Id]
                      FROM [dbo].[User]
                      WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
                      
                      SELECT t0.[Id]
                      FROM [dbo].[User] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[Id] = @Id"
            );
            
            AlterStoredProcedure(
                "dbo.User_Update",
                p => new
                    {
                        Id = p.Int(),
                        Username = p.String(),
                        Password = p.String(),
                        Name = p.String(maxLength: 20),
                        Surname = p.String(maxLength: 20),
                        Gender = p.Int(),
                        Role = p.Int(),
                        IsDeleted = p.Boolean(),
                    },
                body:
                    @"UPDATE [dbo].[User]
                      SET [Username] = @Username, [Password] = @Password, [Name] = @Name, [Surname] = @Surname, [Gender] = @Gender, [Role] = @Role, [IsDeleted] = @IsDeleted
                      WHERE ([Id] = @Id)"
            );
            
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "Surname", c => c.String(nullable: false));
            AlterColumn("dbo.User", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.User", "Username", c => c.String(nullable: false, maxLength: 255));
            throw new NotSupportedException("Scaffolding create or alter procedure operations is not supported in down methods.");
        }
    }
}
