﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Address", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Address", "IsDeleted");
        }
    }
}
