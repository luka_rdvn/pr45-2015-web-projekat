﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateApartment : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (6, 1, 3, 1, 1, 1, 100, '3PM', '11AM')");
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (6, 2, 5, 2, 0, 0, 120, '3PM', '11AM')");
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (7, 3, 2, 1, 1, 1, 110, '3PM', '11AM')");
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (7, 4, 10, 3, 1, 0, 200, '3PM', '11AM')");
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (8, 5, 12, 4, 0, 0, 300, '3PM', '11AM')");
            Sql("INSERT INTO [Apartment](Host_Id, Location_Id, NumberOfGuests, NumberOfRooms, Status, Type, Price, CheckIn, CheckOut) VALUES (8, 6, 6, 3, 0, 0, 250, '3PM', '11AM')");
        }
        
        public override void Down()
        {
        }
    }
}
