﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AmenityApartmentsProp : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Amenity", "Apartment_Id", "dbo.Apartment");
            DropIndex("dbo.Amenity", new[] { "Apartment_Id" });
            CreateTable(
                "dbo.ApartmentAmenity",
                c => new
                    {
                        Apartment_Id = c.Int(nullable: false),
                        Amenity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Apartment_Id, t.Amenity_Id })
                .ForeignKey("dbo.Apartment", t => t.Apartment_Id, cascadeDelete: true)
                .ForeignKey("dbo.Amenity", t => t.Amenity_Id, cascadeDelete: true)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Amenity_Id);
            
            DropColumn("dbo.Amenity", "Apartment_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Amenity", "Apartment_Id", c => c.Int());
            DropForeignKey("dbo.ApartmentAmenity", "Amenity_Id", "dbo.Amenity");
            DropForeignKey("dbo.ApartmentAmenity", "Apartment_Id", "dbo.Apartment");
            DropIndex("dbo.ApartmentAmenity", new[] { "Amenity_Id" });
            DropIndex("dbo.ApartmentAmenity", new[] { "Apartment_Id" });
            DropTable("dbo.ApartmentAmenity");
            CreateIndex("dbo.Amenity", "Apartment_Id");
            AddForeignKey("dbo.Amenity", "Apartment_Id", "dbo.Apartment", "Id");
        }
    }
}
