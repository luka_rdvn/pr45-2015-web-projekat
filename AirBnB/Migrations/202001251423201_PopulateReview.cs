﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateReview : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (2, 1, 5.0, 'Sjajno!')");
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (2, 2, 5.0, 'Fenomenalno!')");
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (3, 1, 5.0, 'Odlicno!')");
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (4, 3, 1.0, 'Uzasno!')");
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (5, 5, 2.0, 'Jako lose!')");
            Sql("INSERT INTO [Review](Apartment_Id, Guest_Id, Rating, Comment) VALUES (6, 4, 4.0, 'Solidno!')");
        }
        
        public override void Down()
        {
        }
    }
}
