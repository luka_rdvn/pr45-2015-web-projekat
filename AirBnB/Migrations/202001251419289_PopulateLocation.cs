﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateLocation : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (80, 80, 1)");
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (85, 80, 2)");
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (70, 76, 3)");
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (45, 80, 4)");
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (68, 92, 5)");
            Sql("INSERT INTO [Location](Latitude, Longitude, Address_Id) VALUES (28, 80, 6)");
        }
        
        public override void Down()
        {
        }
    }
}
