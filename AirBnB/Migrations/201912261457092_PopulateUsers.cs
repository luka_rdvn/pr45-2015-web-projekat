﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateUsers : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('rluka996', 'lukasifra', 'Luka', 'Radovanovic', 0, 2)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('bpredrag96', 'djapesifra', 'Predrag', 'Babic', 0, 2)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('mladjan96', 'mladjansifra', 'Mladen', 'Lukajic', 0, 2)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('vulapalanka', 'vulasifra', 'Stevan', 'Vulic', 0, 2)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('maraca', 'mariosifra', 'Mario', 'Klisanic', 0, 2)");
        }

        public override void Down()
        {
        }
    }
}
