﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserPopulation : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('domacin', 'domacin', 'domacin', 'domacin', 0, 1)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('zorica', 'zorica', 'zorica', 'zorica', 0, 1)");
            Sql("INSERT INTO [USER](Username, Password, Name, Surname, Gender, Role) VALUES ('dragan', 'dragan', 'dragan', 'dragan', 0, 1)");
        }

        public override void Down()
        {
        }
    }
}
