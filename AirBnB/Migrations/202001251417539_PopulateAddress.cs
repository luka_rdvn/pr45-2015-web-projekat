﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateAddress : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Novi Sad', 'Safarikova', '24', 21000)");
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Zrenjanin', 'Kralja Petra', '13', 23000)");
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Beograd', 'Ljutice Bogdana', '2A', 11000)");
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Denver', 'South Federal', '4021', 80110)");
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Ledinci', 'Nikolin do', '4', 21207)");
            Sql("INSERT INTO [Address](City, Street, Number, PostalCode) VALUES ('Sabac', 'Pupinova', '77', 22000)");

        }

        public override void Down()
        {
        }
    }
}
