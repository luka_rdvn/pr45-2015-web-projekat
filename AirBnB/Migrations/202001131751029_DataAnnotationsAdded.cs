﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAnnotationsAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Location", "Address_Id", "dbo.Address");
            DropIndex("dbo.Location", new[] { "Address_Id" });
            AlterColumn("dbo.Address", "Street", c => c.String(nullable: false));
            AlterColumn("dbo.Address", "Number", c => c.String(nullable: false));
            AlterColumn("dbo.Address", "City", c => c.String(nullable: false));
            AlterColumn("dbo.Amenity", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Location", "Address_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Location", "Address_Id");
            AddForeignKey("dbo.Location", "Address_Id", "dbo.Address", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Location", "Address_Id", "dbo.Address");
            DropIndex("dbo.Location", new[] { "Address_Id" });
            AlterColumn("dbo.Location", "Address_Id", c => c.Int());
            AlterColumn("dbo.Amenity", "Name", c => c.String());
            AlterColumn("dbo.Address", "City", c => c.String());
            AlterColumn("dbo.Address", "Number", c => c.String());
            AlterColumn("dbo.Address", "Street", c => c.String());
            CreateIndex("dbo.Location", "Address_Id");
            AddForeignKey("dbo.Location", "Address_Id", "dbo.Address", "Id");
        }
    }
}
