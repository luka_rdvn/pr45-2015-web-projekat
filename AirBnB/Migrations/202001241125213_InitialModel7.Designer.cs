﻿// <auto-generated />
namespace AirBnB.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class InitialModel7 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialModel7));
        
        string IMigrationMetadata.Id
        {
            get { return "202001241125213_InitialModel7"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
