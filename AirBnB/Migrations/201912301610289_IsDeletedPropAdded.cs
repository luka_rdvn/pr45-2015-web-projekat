﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsDeletedPropAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "IsDeleted");
        }
    }
}
