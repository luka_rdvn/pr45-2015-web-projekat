﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Image", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Review", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Review", "IsDeleted");
            DropColumn("dbo.Image", "IsDeleted");
        }
    }
}
