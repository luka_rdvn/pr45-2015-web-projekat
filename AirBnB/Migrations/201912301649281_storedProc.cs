﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class storedProc : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
                "dbo.User_Insert",
                p => new
                    {
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        Name = p.String(),
                        Surname = p.String(),
                        Gender = p.Int(),
                        Role = p.Int(),
                        IsDeleted = p.Boolean(),
                    },
                body:
                    @"INSERT [dbo].[User]([Username], [Password], [Name], [Surname], [Gender], [Role], [IsDeleted])
                      VALUES (@Username, @Password, @Name, @Surname, @Gender, @Role, @IsDeleted)
                      
                      DECLARE @Id int
                      SELECT @Id = [Id]
                      FROM [dbo].[User]
                      WHERE @@ROWCOUNT > 0 AND [Id] = scope_identity()
                      
                      SELECT t0.[Id]
                      FROM [dbo].[User] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[Id] = @Id"
            );
            
            CreateStoredProcedure(
                "dbo.User_Update",
                p => new
                    {
                        Id = p.Int(),
                        Username = p.String(maxLength: 255),
                        Password = p.String(),
                        Name = p.String(),
                        Surname = p.String(),
                        Gender = p.Int(),
                        Role = p.Int(),
                        IsDeleted = p.Boolean(),
                    },
                body:
                    @"UPDATE [dbo].[User]
                      SET [Username] = @Username, [Password] = @Password, [Name] = @Name, [Surname] = @Surname, [Gender] = @Gender, [Role] = @Role, [IsDeleted] = @IsDeleted
                      WHERE ([Id] = @Id)"
            );
            
            CreateStoredProcedure(
                "dbo.User_Delete",
                p => new
                    {
                        Id = p.Int(),
                    },
                body:
                    @"DELETE [dbo].[User]
                      WHERE ([Id] = @Id)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.User_Delete");
            DropStoredProcedure("dbo.User_Update");
            DropStoredProcedure("dbo.User_Insert");
        }
    }
}
