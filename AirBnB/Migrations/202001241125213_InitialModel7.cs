﻿namespace AirBnB.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Location", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "IsDeleted");
        }
    }
}
