﻿using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirBnB.ViewModels
{
    public class ApartmentReviewsVM
    {
        public Apartment Apartment { get; set; }
        public List<Review> Reviews { get; set; }
    }
}