﻿using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirBnB.ViewModels
{
    public class ApartmantAmenityVM
    {
        public Apartment Apartment { get; set; }
        public List<Amenity> Amenities { get; set; }
    }
}