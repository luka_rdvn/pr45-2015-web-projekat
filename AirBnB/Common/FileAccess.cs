﻿using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using AirBnB.Models.enums;

namespace AirBnB.Common
{
    public class FileAccess
    {
        public List<string> ReadLines(string filePath)
        {
            //string filePath = @"C:\Users\rluka\OneDrive\Desktop\Web#1\AirBnB\administratori.txt";

            List<string> lines = File.ReadAllLines(filePath).ToList();

            return lines;
        }
    }
}