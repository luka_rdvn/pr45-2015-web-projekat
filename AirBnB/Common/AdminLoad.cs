﻿using AirBnB.Models;
using AirBnB.Models.enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AirBnB.Common
{
    public static class AdminLoad
    {
        public static List<User> admins;
        public static FileAccess access;

        public static void Init()
        {
            admins = new List<User>();
            access = new FileAccess();

            //List<string> lines = access.ReadLines(@"C:\Users\rluka\OneDrive\Desktop\Web#1\AirBnB\administratori.txt");
            string fileName = "administratori.txt";
            string subfolder = "Common";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, subfolder, fileName);
            List<string> lines = access.ReadLines(path);

            foreach (string line in lines)
            {
                string[] entries = line.Split('|');

                User newAdmin = new User();
                newAdmin.Username = entries[0];
                newAdmin.Password = entries[1];
                newAdmin.Name = entries[2];
                newAdmin.Surname = entries[3];
                if (entries[4] == "Male")
                    newAdmin.Gender = Gender.Male;
                else if (entries[4] == "Female")
                    newAdmin.Gender = Gender.Female;
                newAdmin.Role = Role.Admin;
                newAdmin.Id = Int32.Parse(entries[5]);

                admins.Add(newAdmin);
            }
        }
    }
}