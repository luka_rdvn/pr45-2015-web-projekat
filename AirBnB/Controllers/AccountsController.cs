﻿using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AirBnB.Models.enums;
using AirBnB.Common;
using AirBnB.DAL;

using System.Data.Entity;

namespace AirBnB.Controllers
{
    public class AccountsController : Controller
    {
        private MyDbContext _context = new MyDbContext();

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET Accounts/Register, registraciona forma
        public ActionResult Register() //registracija GOSTIJU
        {
            if (Session["Id"] == null) //ukoliko niko nije ulogovan, dozvoli pristup registracionoj formi
                return View();
            else
                return View("AlreadyLoggedInError");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(User guest) //registracija GOSTIJU
        {
            //var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToArray();
            if (!ModelState.IsValid)
                return View();

            var temp = _context.Users.SingleOrDefault(t => t.Username == guest.Username);
            if (temp != null)
                return View("AlreadyExists");

            _context.Users.Add(guest);
            _context.SaveChanges();
            return View("Success");
        }

        public ActionResult Login() 
        {
            if (Session["Id"] == null)  //ako niko nije ulogovan, prikazi login formu
                return View();
            else
                return View("AlreadyLoggedInError");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string password)
        {
            //pretraga da li je medju "obicnim" korisnicima
            var usr = _context.Users.SingleOrDefault(u => u.Username == username && u.Password == password);
            if (usr != null)
            {
                Session["Id"] = usr.Id.ToString();
                Session["Username"] = usr.Username;
                Session["Role"] = usr.Role.ToString();
                return View("LoggedIn");
            }
            //ako nije, pretraga da li je medju adminima
            usr = AdminLoad.admins.SingleOrDefault(u => u.Username == username && u.Password == password);
            if (usr != null)
            {
                Session["Id"] = usr.Id.ToString();
                Session["Username"] = usr.Username.ToString();
                Session["Role"] = usr.Role.ToString();
                return View("LoggedIn");
            }
            //ako korisnik ne postoji
            return View("WrongCredentials");
        }

        public ActionResult LogOut()
        {
            if (Session["Id"] != null)
            {
                Session["Id"] = null;
                Session["Username"] = null;
                Session["Role"] = null;
                Session.Abandon();
                return View("Success");
            }
            return HttpNotFound();
        }

        public ActionResult ListUsers(string option, string search, string sort)
        {         
            //listu svih korisnika mogu da vide samo admini
            if (Session["Id"] != null && (string)Session["Role"] == "Admin")
            {
                List<User> users = _context.Users.Where(x => x.IsDeleted == false).ToList();

                #region sort
                ViewBag.SortByUsername = sort == "username" ? "username desc" : "username";
                ViewBag.SortByName = sort == "name" ? "name desc" : "name";
                ViewBag.SortBySurname = sort == "surname" ? "surname desc" : "surname";
                ViewBag.SortByGender = sort == "gender" ? "gender desc" : "gender";
                ViewBag.SortByRole = sort == "role" ? "role desc" : "role";
                #endregion sort
                
                if (option == "Username")
                {
                    users = users.Where(x => x.Username.Contains(search) || search == null).ToList();
                }
                else if (option == "Gender")
                {
                    users = users.Where(x => x.Gender.ToString().ToLower() == search.ToLower() || search == null).ToList();
                }
                else if (option == "Role")
                {
                    users = users.Where(x => x.Role.ToString().ToLower() == search.ToLower() || search == null).ToList();
                }

                var users2 = users.AsQueryable();

                #region sortswitch
                switch (sort)
                {
                    case "username desc":
                        users2 = users2.OrderByDescending(x => x.Username);
                        break;
                    case "username":
                        users2 = users2.OrderBy(x => x.Username);
                        break;
                    case "name desc":
                        users2 = users2.OrderByDescending(x => x.Name);
                        break;
                    case "name":
                        users2 = users2.OrderBy(x => x.Name);
                        break;
                    case "surname desc":
                        users2 = users2.OrderByDescending(x => x.Surname);
                        break;
                    case "surname":
                        users2 = users2.OrderBy(x => x.Surname);
                        break;
                    case "gender desc":
                        users2 = users2.OrderByDescending(x => x.Gender);
                        break;
                    case "gender":
                        users2 = users2.OrderBy(x => x.Gender);
                        break;
                    case "role desc":
                        users2 = users2.OrderByDescending(x => x.Role);
                        break;
                    case "role":
                        users2 = users2.OrderBy(x => x.Role);
                        break;
                    default:
                        users2 = users2.OrderBy(x => x.Id);
                        break;
                }
                #endregion sortswitch

                return View(users2.ToList());
            }
            return View("NotLoggedInError");
        }

        public ActionResult ListAdmins(string option, string search, string sort)
        {
            //listu admina mogu da vide samo ostali admini
            if (Session["Id"] != null && (string)Session["Role"] == "Admin")
            {
                var admins = AdminLoad.admins;
                #region sort
                ViewBag.SortByUsername = sort == "username" ? "username desc" : "username";
                ViewBag.SortByName = sort == "name" ? "name desc" : "name";
                ViewBag.SortBySurname = sort == "surname" ? "surname desc" : "surname";
                ViewBag.SortByGender = sort == "gender" ? "gender desc" : "gender";
                ViewBag.SortByRole = sort == "role" ? "role desc" : "role";
                #endregion sort
                if (option == "Username")
                {
                    admins = admins.Where(x => x.Username.Contains(search) || search == null).ToList();
                }
                else if (option == "Gender")
                {
                    admins = admins.Where(x => x.Gender.ToString().ToLower() == search.ToLower() || search == null).ToList();
                }

                var admins2 = admins.AsQueryable();
                #region sortswitch
                switch (sort)
                {
                    case "username desc":
                        admins2 = admins2.OrderByDescending(x => x.Username);
                        break;
                    case "username":
                        admins2 = admins2.OrderBy(x => x.Username);
                        break;
                    case "name desc":
                        admins2 = admins2.OrderByDescending(x => x.Name);
                        break;
                    case "name":
                        admins2 = admins2.OrderBy(x => x.Name);
                        break;
                    case "surname desc":
                        admins2 = admins2.OrderByDescending(x => x.Surname);
                        break;
                    case "surname":
                        admins2 = admins2.OrderBy(x => x.Surname);
                        break;
                    case "gender desc":
                        admins2 = admins2.OrderByDescending(x => x.Gender);
                        break;
                    case "gender":
                        admins2 = admins2.OrderBy(x => x.Gender);
                        break;
                    default:
                        admins2 = admins2.OrderBy(x => x.Id);
                        break;
                }
                #endregion sortswitch

                return View(admins2.ToList());
            }
            return View("NotLoggedInError");
        }

        public ActionResult Edit()
        {
            if (Session["id"] == null)
                return HttpNotFound();

            string idString = (string)Session["Id"];
            int tempId = int.Parse(idString);
            var user = _context.Users.SingleOrDefault(u => u.Id == tempId);

            if (user != null)
            {   //provera da li je medju reg. userima
                ViewBag.Role = user.Role.ToString();
                return View(user);              
            }
            user = AdminLoad.admins.SingleOrDefault(u => u.Id == tempId);
            if (user != null)//ako nije, provera da li je medju adminima
            {
                ViewBag.Role = user.Role.ToString();
                return View(user);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(User user) //edit ne radi za admine
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            /*var ind = _context.Users.ToList().FindIndex(x => x.Username == user.Username);
            if (ind >= 0)
                return View("AlreadyExists");*/
            var list = _context.Users.ToList();
            list.RemoveAll(x => x.Id == user.Id);
            var ind = list.FindIndex(x => x.Username == user.Username);
            if (ind >= 0)
                return View("AlreadyExists");


            var userInDb = _context.Users.SingleOrDefault(u => u.Id == user.Id);
            if (userInDb == null)
                return HttpNotFound();

            userInDb.Username = user.Username;
            userInDb.Password = user.Password;
            userInDb.Name = user.Name;
            userInDb.Surname = user.Surname;
            _context.SaveChanges();            
            return View("Success");
        }

        public ActionResult MakeHost(int id)
        {
            if (Session["Id"] != null && (string)Session["Role"] == "Admin")
            {
                var userInDb = _context.Users.SingleOrDefault(u => u.Id == id);
                if (userInDb == null)
                    return HttpNotFound();
                userInDb.Role = Role.Host;
                _context.SaveChanges();
                return RedirectToAction("ListUsers");
            }
            return View("NotLoggedInError");
        }

        public ActionResult MakeNewHost()
        {
            if (Session["Id"] != null && (string)Session["Role"] == "Admin")
            {
                return View(AdminLoad.admins);
            }
            return View("NotLoggedInError");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakeNewHost(User host)
        {          
            if (!ModelState.IsValid)
                return View();
            var temp = _context.Users.SingleOrDefault(t => t.Username == host.Username);
            if (temp != null)
                return View("AlreadyExists");

            host.Role = Role.Host;
            _context.Users.Add(host);
            _context.SaveChanges();
            return View("Success");
        }

        // GET: Accounts/Delete/5
        public ActionResult Delete(int id)  //SOFT DELETE
        {
            var userInDb = _context.Users.SingleOrDefault(u => u.Id == id);
            if (userInDb == null)
                return HttpNotFound();
            userInDb.IsDeleted = true;
            _context.Users.Remove(userInDb);
            _context.SaveChanges();

            return RedirectToAction("ListUsers");
        }

        public ActionResult ListMyApartments(string option, string search, string search2, string sort, string typeFilter, string statusFilter, string submit, string amenityFilter)
        {           
            if (Session["Id"] != null && (string)Session["Role"] == "Host")
            {
                #region sort
                ViewBag.SortByCity = sort == "city" ? "city desc" : "city";
                ViewBag.SortByPrice = sort == "price" ? "price desc" : "price";
                ViewBag.SortByType = sort == "type" ? "type desc" : "type";
                ViewBag.SortByStreet = sort == "street" ? "street desc" : "street";
                ViewBag.SortByGuests = sort == "guests" ? "guests desc" : "guests";
                ViewBag.SortByRooms = sort == "rooms" ? "rooms desc" : "rooms";
                ViewBag.SortByHost = sort == "host" ? "host desc" : "host";
                ViewBag.SortByStatus = sort == "status" ? "status desc" : "status";
                #endregion sort

                ViewBag.AptTypes = Enum.GetValues(typeof(ApartmentType)).Cast<ApartmentType>();
                ViewBag.Statuses = Enum.GetValues(typeof(ApartmentStatus)).Cast<ApartmentStatus>();
                ViewBag.Amnts = _context.Amenities.ToList();

                string tempUsername = (string)Session["Username"];
                var apartments = _context.Apartments.Include(x => x.Amenities).ToList();
                var user = _context.Users.SingleOrDefault(u => u.Username == tempUsername);

                var temp = _context.Apartments.Include("Host").Include("Amenities").Include("Location.Address").ToList();
                var apts = temp.Where(x => x.Host == user && x.IsDeleted == false).ToList();
                if (apts == null)
                {
                    user.ApartmentsForRent.Clear();
                    return View(user);
                }

                int tmp;

                if (option == "Location")
                    apts = apts.Where(x => x.Location.Address.City.Contains(search)
                                || x.Location.Address.Street.Contains(search)
                                || search == null).ToList();
                else if (option == "Guests")
                    if (int.TryParse(search, out tmp))
                        apts = apts.Where(x => x.NumberOfGuests == Int32.Parse(search) || search == null).ToList();
                    else
                        apts = apts.ToList();
                else if (option == "Rooms")
                    if (int.TryParse(search, out tmp) && int.TryParse(search2, out tmp))
                        apts = apts.Where(x => x.NumberOfRooms >= Int32.Parse(search) && x.NumberOfRooms <= Int32.Parse(search2)).ToList();
                    else
                        apts = apts.ToList();
                else if (option == "Price")
                    if (int.TryParse(search, out tmp) && int.TryParse(search2, out tmp))
                        apts = apts.Where(x => x.Price >= Int32.Parse(search) && x.Price <= Int32.Parse(search2)).ToList();
                    else
                        apts = apts.ToList();

                var apts2 = apts.AsQueryable();

                #region sortswitch
                switch (sort)
                {
                    case "city desc":
                        apts2 = apts2.OrderByDescending(x => x.Location.Address.City);
                        break;
                    case "city":
                        apts2 = apts2.OrderBy(x => x.Location.Address.City);
                        break;
                    case "price desc":
                        apts2 = apts2.OrderByDescending(x => x.Price);
                        break;
                    case "price":
                        apts2 = apts2.OrderBy(x => x.Price);
                        break;
                    case "type desc":
                        apts2 = apts2.OrderByDescending(x => x.Type.ToString());
                        break;
                    case "type":
                        apts2 = apts2.OrderBy(x => x.Type);
                        break;
                    case "street desc":
                        apts2 = apts2.OrderByDescending(x => x.Location.Address.Street);
                        break;
                    case "street":
                        apts2 = apts2.OrderBy(x => x.Location.Address.Street);
                        break;
                    case "guests desc":
                        apts2 = apts2.OrderByDescending(x => x.NumberOfGuests);
                        break;
                    case "guests":
                        apts2 = apts2.OrderBy(x => x.NumberOfGuests);
                        break;
                    case "rooms desc":
                        apts2 = apts2.OrderByDescending(x => x.NumberOfRooms);
                        break;
                    case "rooms":
                        apts2 = apts2.OrderBy(x => x.NumberOfRooms);
                        break;
                    case "host desc":
                        apts2 = apts2.OrderByDescending(x => x.Host);
                        break;
                    case "host":
                        apts2 = apts2.OrderBy(x => x.Host);
                        break;
                    case "status desc":
                        apts2 = apts2.OrderByDescending(x => x.Status);
                        break;
                    case "status":
                        apts2 = apts2.OrderBy(x => x.Status);
                        break;
                    default:
                        apts2 = apts2.OrderBy(x => x.Id);
                        break;
                }
                #endregion sortswitch

                if (submit == "TypeFilter")
                {
                    if (typeFilter == "EntirePlace")
                    {
                        user.ApartmentsForRent = apts.Where(x => x.Type == ApartmentType.EntirePlace).ToList();
                        return View(user);
                    }
                    else if (typeFilter == "Room")
                    {
                        user.ApartmentsForRent = apts.Where(x => x.Type == ApartmentType.Room).ToList();
                        return View(user);
                    }
                }
                else if (submit == "StatusFilter")
                {
                    if (statusFilter == "Active")
                    {
                        user.ApartmentsForRent = apts.Where(x => x.Status == ApartmentStatus.Active).ToList();
                        return View(user);
                    }
                    else if (statusFilter == "Inactive")
                    {
                        user.ApartmentsForRent = apts.Where(x => x.Status == ApartmentStatus.Inactive).ToList();
                        return View(user);
                    }
                }
                else if (submit == "AmenityFilter")
                {
                    foreach (var item in _context.Amenities.ToList())
                    {
                        if (amenityFilter == item.Name)
                        {
                            user.ApartmentsForRent = apts.Where(x => x.Amenities.Contains(item)).ToList();
                            return View(user);
                        }
                    }
                }

                user.ApartmentsForRent = apts2.ToList();

                return View(user);
            }
            else
                return View("NotLoggedInError");
        }
    }
}