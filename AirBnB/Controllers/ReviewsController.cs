﻿using AirBnB.DAL;
using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AirBnB.Controllers
{
    public class ReviewsController : Controller
    {
        private MyDbContext _context = new MyDbContext();

        // GET: Reviews
        public ActionResult Index()
        {
            return View();
        }
            
        public ActionResult Add()   //komentare na apartmane ostavljaju gosti
        {
            var apartments = _context.Apartments;

            if (Session["Id"] != null && (string)Session["Role"] == "Guest")
            {
                return View();
            }

            return HttpNotFound();
        }

        [HttpPost] 
        public ActionResult Add(Review review, int apartmentId)
        {
            if (!ModelState.IsValid)
                return View();

            string tempUsername = (string)Session["Username"];
            var user = _context.Users.SingleOrDefault(u => u.Username == tempUsername);

            var reviews = _context.Reviews.Include("Apartment").Include("Guest").ToList();
            var apartment = _context.Apartments.ToList().SingleOrDefault(x => x.Id == apartmentId);
            if (apartment == null)
                return HttpNotFound();
            if (apartment.Reviews == null)
                apartment.Reviews = new List<Review>();

            review.Apartment = apartment;
            review.Guest = user;
            reviews.Add(review);

            apartment.Reviews.Add(review);
            //_context.Reviews.Add(review); moze li bez ovoga?
            _context.SaveChanges();

            return View();
        }

        public ActionResult ListReviews()   //sve komentare vide admini ili domacini
        {
            if ((string)Session["Role"] == "Admin") //admin moze da vidi sve komentare za sve apartmane
                return View("ListReviewsAdmin", _context.Reviews.Include("Guest").Include("Apartment").Where(x => x.IsDeleted == false).ToList());
            else if ((string)Session["Role"] == "Host")
            {
                return View(_context.Reviews.Include("Guest").Include("Apartment").Where(x => x.IsDeleted == false).ToList());
            }
            else //ovde prikaz za goste, filtrirani komentari
                return View(_context.Reviews.Include("Guest").Include("Apartment").Where(x => x.IsHidden == false && x.IsDeleted == false).ToList());
        }

        public ActionResult Hide(int id)
        {
            var review = _context.Reviews.Include("Apartment").SingleOrDefault(x => x.Id == id);
            if (review == null)
                return HttpNotFound();

            var apt = review.Apartment;
            review.IsHidden = review.IsHidden ? false : true;
            _context.SaveChanges();
            return RedirectToAction("EditAptReviews", "Apartments", new { id = apt.Id });
        }
    }
}