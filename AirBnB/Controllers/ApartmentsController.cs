﻿using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AirBnB.Controllers;
using AirBnB.DAL;
using AirBnB.Models.enums;
using System.IO;
using System.Configuration;
using AirBnB.ViewModels;

namespace AirBnB.Controllers
{
    public class ApartmentsController : Controller
    {
        private MyDbContext _context = new MyDbContext();

        public ActionResult AddApartment()
        {
            if (Session["Id"] != null && (string)Session["Role"] == "Host")
            {
                ApartmantAmenityVM vm = new ApartmantAmenityVM {
                    Apartment = new Apartment(),
                    Amenities = _context.Amenities.Where(x => x.IsDeleted == false).ToList()
                };
                return View(vm);
            }
                
            else
                return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddApartment(ApartmantAmenityVM vm, string[] selectedCheckboxes)
        {
            if (!ModelState.IsValid)
            {
                ApartmantAmenityVM temp = new ApartmantAmenityVM
                {
                    Apartment = new Apartment(),
                    Amenities = _context.Amenities.ToList()
                };
                return View(temp);
            }

            vm.Apartment.Status = ApartmentStatus.Inactive; //inicijalno je svaki apartman neaktivan

            string currentUsername = (string)Session["Username"];
            var currentUser = _context.Users.SingleOrDefault(u => u.Username == currentUsername);
            vm.Apartment.Host = currentUser;

            vm.Apartment.Reviews = new List<Review>();  //inicijalno, apartman nema nikakvih komentara

            #region images
            vm.Apartment.Images = new List<Image>();
            List<Image> imgs = vm.Apartment.Images;
            if (imgs == null)
                imgs = new List<Image>();
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);

                        file.SaveAs(path);
                        Image tempImg = new Image(fileName, path);
                        imgs.Add(tempImg);
                    }
                }
            }
            vm.Apartment.Images = imgs;
            #endregion images

            #region amenities
            vm.Apartment.Amenities = new List<Amenity>();
            if (selectedCheckboxes != null)
            {
                List<int> ids = selectedCheckboxes.Select(int.Parse).ToList();
                var allAmenities = _context.Amenities.ToList();
                foreach (int index in ids)
                {
                    var a = allAmenities.Find(x => x.Id == index);
                    vm.Apartment.Amenities.Add(a);
                }
            }        
            #endregion amenities

            //mozda moze i bez ove linije?
            //currentUser.ApartmentsForRent.Add(vm.Apartment);
            _context.Apartments.Add(vm.Apartment);
            _context.SaveChanges();

            return View("Success");
        }

        public ActionResult ListAllApartments(string option, string search, string search2, string sort, string typeFilter, string statusFilter, string submit, string amenityFilter)
        {                     
            if (Session["Id"] != null && (string)Session["Role"] == "Admin")
            {
                var apts = _context.Apartments.Include("Host").Include("Amenities").Include("Location.Address").Where(x => x.IsDeleted == false).ToList();

                #region sort
                ViewBag.SortByCity = sort == "city" ? "city desc" : "city";
                ViewBag.SortByPrice = sort == "price" ? "price desc" : "price";
                ViewBag.SortByType = sort == "type" ? "type desc" : "type";
                ViewBag.SortByStreet = sort == "street" ? "street desc" : "street";
                ViewBag.SortByGuests = sort == "guests" ? "guests desc" : "guests";
                ViewBag.SortByRooms = sort == "rooms" ? "rooms desc" : "rooms";
                ViewBag.SortByHost = sort == "host" ? "host desc" : "host";
                ViewBag.SortByStatus = sort == "status" ? "status desc" : "status";
                #endregion sort

                ViewBag.AptTypes = Enum.GetValues(typeof(ApartmentType)).Cast<ApartmentType>();
                ViewBag.Statuses = Enum.GetValues(typeof(ApartmentStatus)).Cast<ApartmentStatus>();
                ViewBag.Amnts = _context.Amenities.ToList();

                int temp;

                if (option == "Location")
                        apts = apts.Where(x => x.Location.Address.City.Contains(search)
                                || x.Location.Address.Street.Contains(search)
                                || search == null).ToList();                    
                else if (option == "Guests")
                    if (int.TryParse(search, out temp))
                        apts = apts.Where(x => x.NumberOfGuests == Int32.Parse(search) || search == null).ToList();
                    else
                        apts = apts.ToList();
                else if (option == "Rooms")
                    if (int.TryParse(search, out temp) && int.TryParse(search2, out temp))
                        apts = apts.Where(x => x.NumberOfRooms >= Int32.Parse(search) && x.NumberOfRooms <= Int32.Parse(search2)).ToList();
                    else
                        apts = apts.ToList();
                else if (option == "Price")
                    if (int.TryParse(search, out temp) && int.TryParse(search2, out temp))
                        apts = apts.Where(x => x.Price >= Int32.Parse(search) && x.Price <= Int32.Parse(search2)).ToList();
                    else
                        apts = apts.ToList();

                var apts2 = apts.AsQueryable();

                #region sortswitch
                switch (sort)
                {
                    case "city desc":
                        apts2 = apts2.OrderByDescending(x => x.Location.Address.City);
                        break;
                    case "city":
                        apts2 = apts2.OrderBy(x => x.Location.Address.City);
                        break;
                    case "price desc":
                        apts2 = apts2.OrderByDescending(x => x.Price);
                        break;
                    case "price":
                        apts2 = apts2.OrderBy(x => x.Price);
                        break;
                    case "type desc":
                        apts2 = apts2.OrderByDescending(x => x.Type.ToString());
                        break;
                    case "type":
                        apts2 = apts2.OrderBy(x => x.Type);
                        break;
                    case "street desc":
                        apts2 = apts2.OrderByDescending(x => x.Location.Address.Street);
                        break;
                    case "street":
                        apts2 = apts2.OrderBy(x => x.Location.Address.Street);
                        break;
                    case "guests desc":
                        apts2 = apts2.OrderByDescending(x => x.NumberOfGuests);
                        break;
                    case "guests":
                        apts2 = apts2.OrderBy(x => x.NumberOfGuests);
                        break;
                    case "rooms desc":
                        apts2 = apts2.OrderByDescending(x => x.NumberOfRooms);
                        break;
                    case "rooms":
                        apts2 = apts2.OrderBy(x => x.NumberOfRooms);
                        break;
                    case "host desc":
                        apts2 = apts2.OrderByDescending(x => x.Host.Username);
                        break;
                    case "host":
                        apts2 = apts2.OrderBy(x => x.Host.Username);
                        break;
                    case "status desc":
                        apts2 = apts2.OrderByDescending(x => x.Status);
                        break;
                    case "status":
                        apts2 = apts2.OrderBy(x => x.Status);
                        break;
                    default:
                        apts2 = apts2.OrderBy(x => x.Id);
                        break;
                }
                #endregion sortswitch

                if (submit == "TypeFilter")
                {
                    if (typeFilter == "EntirePlace")
                        return View(apts.Where(x => x.Type == ApartmentType.EntirePlace));
                    else if (typeFilter == "Room")
                        return View(apts.Where(x => x.Type == ApartmentType.Room));
                }
                else if (submit == "StatusFilter")
                {
                    if (statusFilter == "Active")
                        return View(apts.Where(x => x.Status == ApartmentStatus.Active));
                    else if (statusFilter == "Inactive")
                        return View(apts.Where(x => x.Status == ApartmentStatus.Inactive));
                }
                else if (submit == "AmenityFilter")
                {
                    foreach (var item in _context.Amenities.ToList())
                        if (amenityFilter == item.Name)
                            return View(apts.Where(x => x.Amenities.Contains(item)));
                }

                return View(apts2.ToList());
            }
            else
                return HttpNotFound();
        }

        public ActionResult EditApartmentVM(int id)
        {
            ApartmantAmenityVM vm = new ApartmantAmenityVM();

            var apts = _context.Apartments.Include("Images").Include("Location.Address").ToList();
            var apt = apts.SingleOrDefault(a => a.Id == id);
            if (apt == null)
                return HttpNotFound();

            vm.Apartment = apt;
            vm.Amenities = _context.Amenities.Include("Apartments").Where(x => x.IsDeleted == false).ToList();

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditApartment(Apartment apartment)
        {
            if (!ModelState.IsValid)
            {
                ApartmantAmenityVM temp = new ApartmantAmenityVM
                {
                    Apartment = new Apartment(),
                    Amenities = _context.Amenities.Where(x => x.IsDeleted == false).ToList()
                };
                return View("EditApartmentVM", temp);
            }

            var aptInDb = _context.Apartments.Include("Location.Address").SingleOrDefault(u => u.Id == apartment.Id);
      
            #region datainit
            aptInDb.Type = apartment.Type;
            aptInDb.NumberOfGuests = apartment.NumberOfGuests;
            aptInDb.NumberOfRooms = apartment.NumberOfRooms;
            aptInDb.Price = apartment.Price;
            aptInDb.CheckIn = apartment.CheckIn;
            aptInDb.CheckOut = apartment.CheckOut;
            aptInDb.Status = apartment.Status;
            //aptInDb.Location.Address = apartment.Location.Address;
            aptInDb.Location.Address.City = apartment.Location.Address.City;
            aptInDb.Location.Address.Number = apartment.Location.Address.Number;
            aptInDb.Location.Address.PostalCode = apartment.Location.Address.PostalCode;
            aptInDb.Location.Address.Street = apartment.Location.Address.Street;
            aptInDb.Location.Longitude = apartment.Location.Longitude;
            aptInDb.Location.Latitude = apartment.Location.Latitude;
            #endregion datainit

            _context.SaveChanges();
            return View("Success");
        }

        public ActionResult ListApartmentsLoggedOut(string option, string search, string search2, string sort, string typeFilter, string submit, string amenityFilter)
        {
            var apts = _context.Apartments.Include("Host").Include("Location.Address").Include("Amenities").Where(x => x.Status == ApartmentStatus.Active && x.IsDeleted == false).ToList();

            #region sort
            ViewBag.SortByCity = sort == "city" ? "city desc" : "city";
            ViewBag.SortByPrice = sort == "price" ? "price desc" : "price";
            ViewBag.SortByType = sort == "type" ? "type desc" : "type";
            ViewBag.SortByStreet = sort == "street" ? "street desc" : "street";
            ViewBag.SortByGuests = sort == "guests" ? "guests desc" : "guests";
            ViewBag.SortByRooms = sort == "rooms" ? "rooms desc" : "rooms";
            ViewBag.SortByHost = sort == "host" ? "host desc" : "host";
            #endregion sort

            ViewBag.AptTypes = Enum.GetValues(typeof(ApartmentType)).Cast<ApartmentType>();
            ViewBag.Amnts = _context.Amenities.ToList();

            int temp;

            if (option == "Location")
                apts = apts.Where(x => x.Location.Address.City.Contains(search)
                            || x.Location.Address.Street.Contains(search)
                            || search == null).ToList();

            else if (option == "Guests")
                if (int.TryParse(search, out temp))
                    apts = apts.Where(x => x.NumberOfGuests == int.Parse(search) || search == null).ToList();
                else
                    apts = apts.ToList();
            else if (option == "Rooms")
                if (int.TryParse(search, out temp) && int.TryParse(search2, out temp))
                    apts = apts.Where(x => x.NumberOfRooms >= int.Parse(search) && x.NumberOfRooms <= int.Parse(search2)).ToList();
                else
                    apts = apts.ToList();
            else if (option == "Price")
                if (int.TryParse(search, out temp) && int.TryParse(search2, out temp))
                    apts = apts.Where(x => x.Price >= int.Parse(search) && x.Price <= int.Parse(search2)).ToList();
                else
                    apts = apts.ToList();

            var apts2 = apts.AsQueryable();

            #region sortswitch
            switch (sort)
            {
                case "city desc":
                    apts2 = apts2.OrderByDescending(x => x.Location.Address.City);
                    break;
                case "city":
                    apts2 = apts2.OrderBy(x => x.Location.Address.City);
                    break;
                case "price desc":
                    apts2 = apts2.OrderByDescending(x => x.Price);
                    break;
                case "price":
                    apts2 = apts2.OrderBy(x => x.Price);
                    break;
                case "type desc":
                    apts2 = apts2.OrderByDescending(x => x.Type.ToString());
                    break;
                case "type":
                    apts2 = apts2.OrderBy(x => x.Type);
                    break;
                case "street desc":
                    apts2 = apts2.OrderByDescending(x => x.Location.Address.Street);
                    break;
                case "street":
                    apts2 = apts2.OrderBy(x => x.Location.Address.Street);
                    break;
                case "guests desc":
                    apts2 = apts2.OrderByDescending(x => x.NumberOfGuests);
                    break;
                case "guests":
                    apts2 = apts2.OrderBy(x => x.NumberOfGuests);
                    break;
                case "rooms desc":
                    apts2 = apts2.OrderByDescending(x => x.NumberOfRooms);
                    break;
                case "rooms":
                    apts2 = apts2.OrderBy(x => x.NumberOfRooms);
                    break;
                case "host desc":
                    apts2 = apts2.OrderByDescending(x => x.Host.Username);
                    break;
                case "host":
                    apts2 = apts2.OrderBy(x => x.Host.Username);
                    break;
                default:
                    apts2 = apts2.OrderBy(x => x.Id);
                    break;
            }
            #endregion sortswitch

            if (submit == "TypeFilter")
            {
                if (typeFilter == "EntirePlace")
                    return View(apts.Where(x => x.Type == ApartmentType.EntirePlace));
                else if (typeFilter == "Room")
                    return View(apts.Where(x => x.Type == ApartmentType.Room));
            }
            else if (submit == "AmenityFilter")
            {
                foreach (var item in _context.Amenities.ToList())
                {
                    if (amenityFilter == item.Name)
                        return View(apts.Where(x => x.Amenities.Contains(item)));
                }
            }

            return View(apts2.ToList());
        }

        public ActionResult ApartmentDetails(int id)    //svi korisnici mogu videti
        {
            var apt = _context.Apartments.Include("Host").Include("Location.Address").SingleOrDefault(u => u.Id == id);
            if (apt == null)
                return HttpNotFound();

            return View(apt);
        }

        public ActionResult ViewApartmentReviews(int id)//svi korisnici mogu videti filtrirane kom. za apt
        {
            var apt = _context.Apartments.Include("Reviews.Guest").SingleOrDefault(u => u.Id == id);
            if (apt == null)
                return HttpNotFound();

            if (apt.Reviews == null)
                apt.Reviews = new List<Review>();

            /*var reviewsDb = _context.Reviews.Include("Guest").ToList();
            var reviewsToShow = new List<Review>();
            ViewBag.AptId = id;
            foreach (var r in reviewsDb)
            {
                if (r.Apartment == apt)
                {
                    reviewsToShow.Add(r);
                }
            }*/

            ViewBag.AptId = id;
            return View(apt.Reviews.Where(x => x.IsHidden == false));
        }

        public ActionResult ViewApartmentAmenities(int id)//svi korisnici mogu videti
        {
            var apt = _context.Apartments.Include("Amenities").SingleOrDefault(u => u.Id == id);
            if (apt == null)
                return HttpNotFound();

            ViewBag.AptId = id;
            return View(apt.Amenities.Where(x => x.IsDeleted == false));
        }

        public ActionResult ViewApartmentImages(int id)//svi korisnici mogu videti
        {
            var apt = _context.Apartments.Include("Images").SingleOrDefault(a => a.Id == id);
            if (apt == null)
                return HttpNotFound();

            ViewBag.AptId = id;
            //var apt = apts.SingleOrDefault(a => a.Id == id);
            return View(apt.Images);
        }

        public ActionResult EditAptAmenities(int id)
        {
            if (Session["Id"] != null)  //samo ulogovani admini ili domacini mogu vide ovu stranicu
            {
                if ((string)Session["Role"] == "Host" || (string)Session["Role"] == "Admin")
                {
                    ViewBag.AptId = id;

                    ApartmantAmenityVM vm = new ApartmantAmenityVM();

                    var apt = _context.Apartments.Include("Amenities").ToList().SingleOrDefault(a => a.Id == id);
                    if (apt == null)
                        return HttpNotFound();

                    var amenities = _context.Amenities.Where(x => x.IsDeleted == false).ToList().Except(apt.Amenities.Where(x => x.IsDeleted == false));

                    vm.Apartment = apt;
                    vm.Amenities = amenities.ToList();

                    return View(vm);
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAptAmenities(string AptId, string[] selectedCheckboxes)
        {
            Apartment apt = _context.Apartments.Include("Amenities").ToList().SingleOrDefault(a => a.Id == int.Parse(AptId));
            if (apt == null)
                return HttpNotFound();
            if (selectedCheckboxes == null)
            {
                apt.Amenities.Clear();
                _context.SaveChanges();
                return RedirectToAction("EditApartmentVM", new { id = apt.Id });
            }
            List<int> ids = selectedCheckboxes.Select(int.Parse).ToList();
            apt.Amenities.Clear();
            foreach (int index in ids)
            {
                var allAmenities = _context.Amenities.ToList();
                var a = allAmenities.Find(x => x.Id == index);

                apt.Amenities.Add(a);
                _context.SaveChanges();
            }
            return RedirectToAction("EditApartmentVM", new { id = apt.Id });
        }

        public ActionResult EditAptReviews(int id)
        {
            Apartment apt = _context.Apartments.Include("Reviews.Guest").ToList().SingleOrDefault(a => a.Id == id);

            if ((string)Session["Role"] == "Host")
            {
                ViewBag.AptId = id;
                return View(apt);
            }

            else if ((string)Session["Role"] == "Admin")
            {
                ViewBag.AptId = id;
                return View("EditAptReviewsAdmin", apt);
            }
            return View("NotLoggedIn");
        }

        public ActionResult EditAptImages(int id)
        {
            if (Session["Id"] != null)  //samo ulogovani admini ili domacini mogu videti
            {
                if ((string)Session["Role"] == "Host" || (string)Session["Role"] == "Admin")
                {
                    ViewBag.AptId = id;
                    var apts = _context.Apartments.Include("Images").ToList();
                    var apt = apts.SingleOrDefault(a => a.Id == id);
                    return View(apt.Images);
                }
            }
            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAptImages(string AptId, string[] selectedCheckboxes)
        {
            Apartment apt = _context.Apartments.Include("Images").ToList().SingleOrDefault(a => a.Id == int.Parse(AptId));
            if (apt == null)
                return HttpNotFound();

            if (selectedCheckboxes == null)
                return RedirectToAction("EditAptImages", new { id = apt.Id });

            List<int> ids = selectedCheckboxes.Select(int.Parse).ToList();
            var allImages = _context.Images.ToList();

            foreach (int index in ids)
            {
                var img = allImages.Find(x => x.Id == index);
                if (apt.Images.Contains(img))
                {
                    apt.Images.Remove(img);
                    _context.SaveChanges();
                }
            }
            return RedirectToAction("EditAptImages", new { id = apt.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAptImages(string AptId)
        {
            Apartment apt = _context.Apartments.Include("Images").ToList().SingleOrDefault(a => a.Id == int.Parse(AptId));
            if (apt == null)
                return HttpNotFound();

            List<Image> imgs = apt.Images;
            if (imgs == null)
                imgs = new List<Image>();
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Images/"), fileName);

                        file.SaveAs(path);
                        Image tempImg = new Image(fileName, path);
                        imgs.Add(tempImg);
                    }
                }
            }
            apt.Images.Concat(imgs);
            _context.SaveChanges();

            return RedirectToAction("EditAptImages", new { id = apt.Id });
        }

        public ActionResult Delete(int id)//to delete a apartment
        {
            var apartmentDb = _context.Apartments.Include("Location.Address").Include("Images").Include("Reviews").SingleOrDefault(x => x.Id == id);
            if (apartmentDb == null)
                return HttpNotFound();

            apartmentDb.IsDeleted = true;
            //treba obrisati i ocene i slike ovog apartmana ako je on obrisan
            foreach (Review r in apartmentDb.Reviews.ToList())
            {
                r.IsDeleted = true;
            }
            foreach (Image img in apartmentDb.Images.ToList())
            {
                img.IsDeleted = true;
            }

            //_context.Apartments.Remove(apartmentDb);
            _context.SaveChanges();

            return View("Success");
        }
    }
}