﻿using AirBnB.DAL;
using AirBnB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AirBnB.Controllers
{
    public class AmenitiesController : Controller
    {
        private MyDbContext _context = new MyDbContext();

        // GET: Amenities
        public ActionResult Index()
        {
            return View();
        }







        public ActionResult AddAmenity()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddAmenity(Amenity amenity)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            _context.Amenities.Add(amenity);
            _context.SaveChanges();

            return RedirectToAction("ListAmenities");
        }

        public ActionResult ListAmenities()
        {
            return View(_context.Amenities.Where(x => x.IsDeleted == false).ToList());
        }

        public ActionResult Edit(int id)
        {
            var amenities = _context.Amenities.ToList();
            var amenity = amenities.SingleOrDefault(a => a.Id == id);

            return View(amenity);
        }

        [HttpPost]
        public ActionResult Edit(Amenity a)
        {
            if (!ModelState.IsValid)
                return View(a);

            var amenityDb = _context.Amenities.SingleOrDefault(amnt => amnt.Id == a.Id);
            amenityDb.Name = a.Name;
            _context.SaveChanges();

            return View("Success");
        }

        public ActionResult Delete(int id)
        {
            var amenityDb = _context.Amenities.SingleOrDefault(amnt => amnt.Id == id);
            if (amenityDb == null)
                return HttpNotFound();
            amenityDb.IsDeleted = true;
            _context.Amenities.Remove(amenityDb);
            _context.SaveChanges();

            return RedirectToAction("ListAmenities");
        }
    }
}